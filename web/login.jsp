<%@ page import="pack.User" %>
<%@ page import="java.util.ArrayList" %>
<%
    ServletContext context = request.getSession().getServletContext();
    ArrayList<User> regUsers = (ArrayList) context.getAttribute("regUsers");
    System.out.println(regUsers);
    if (regUsers != null && regUsers.size()>0) {
        User user = null;
        String email = request.getParameter("user_email");
        String pass = request.getParameter("user_password");

        if (email != null && !email.isEmpty()) {
           user = new User(email, pass);
        }

        for (User u : regUsers) {
            if ((u.getEmail().equals(email) && (u.getPass().equals(pass)))) {
                context.setAttribute("curUser", user);
                response.sendRedirect("news.jsp");
            } else {
                response.sendRedirect("log.jsp");
            }
        }
    }else {
        response.sendRedirect("registration.jsp");
    }

%>