<%@ page import="pack.User" %>
<%
    ServletContext context = request.getSession().getServletContext();
    User curUser = (User)context.getAttribute("curUser");
    if(curUser==null){
        context.setAttribute("curUser", new User("",""));
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SLOW NEWS</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
<div id="header">
    <div id="SlowNews">
        <h1>Slooooow News</h1>
    </div>
    <div id="userMail">
        <h3>${curUser.email}<form action="logOut.jsp" method="post"><button><b>Log out</b></button></form></h3>
    </div>
</div>
<div id="sidebar">
    <h2><a href="log.jsp">Login</a></h2>

    <h2><a href="registration.jsp">Registration</a></h2>

    <h2><a href="news.jsp">News</a></h2>

    <h2><a href="archive.jsp">Archive</a></h2>
</div>
<div id="content">
    <h2>Archive</h2></br>
    <div id="block">
        <p>First news</p>
    </div>
    <div id="block">
        <p>Second news</p>
    </div>

</div>
<div id="footer">&copy; Vova</div>
</body>
</html>