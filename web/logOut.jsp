<%@ page import="pack.User" %>
<%
  ServletContext context = request.getSession().getServletContext();
  context.removeAttribute("curUser");
  context.setAttribute("curUser", new User("", ""));
  response.sendRedirect("log.jsp");
%>