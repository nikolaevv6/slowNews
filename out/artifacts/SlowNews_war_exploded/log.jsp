<%@ page import="pack.User" %>
<%
    ServletContext context = request.getSession().getServletContext();
    User curUser = (User)context.getAttribute("curUser");
    if(curUser==null){
        context.setAttribute("curUser", new User("",""));
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SLOW NEWS</title>
    <link rel="stylesheet" href="main.css">

</head>
<body>
<div id="header">
    <div id="SlowNews">
        <h1>Slooooow News</h1>
    </div>
    <div id="userMail">
        <h3>${curUser.email}<form action="logOut.jsp" method="post"><button><b>Log out</b></button></form></h3>
    </div>
</div>
<div id="sidebar">
    <h2><a href="log.jsp">Login</a></h2>

    <h2><a href="registration.jsp">Registration</a></h2>

    <h2><a href="news.jsp">News</a></h2>

    <h2><a href="archive.jsp">Archive</a></h2>
</div>
<div id="content">
    <form action="login.jsp" method="post">

        <h2>Sing in</h2><br/>

        <div class="left">

            <label for="mail">E-mail:</label><br/>
            <input type="email" id="mail" name="user_email" value=""><br/>
            <label for="password">Password:</label><br/>
            <input type="password" id="password" name="user_password" value="">
        </div>
        <br/>

        <button><b>Sing in</b></button>

    </form>
</div>
<div id="footer">&copy; Vova</div>
</body>
</html>
