<%@ page import="pack.User" %>
<%
    ServletContext context = request.getSession().getServletContext();
    User curUser = (User)context.getAttribute("curUser");
    if(curUser==null){
        context.setAttribute("curUser", new User("",""));
    }
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SLOW NEWS</title>
    <link rel="stylesheet" href="main.css">
</head>
<body>
<div id="header">
    <div id="SlowNews">
        <h1>Slooooow News</h1>
    </div>
    <div id="userMail">
        <h3>${curUser.email}<form action="logOut.jsp" method="post"><button><b>Log out</b></button></form></h3>
    </div>
</div>
<div id="sidebar">
    <h2><a href="log.jsp">Login</a></h2>

    <h2><a href="registration.jsp">Registration</a></h2>

    <h2><a href="news.jsp">News</a></h2>

    <h2><a href="archive.jsp">Archive</a></h2>
</div>
<div id="content">
    <form action="reg.jsp" method="post">

        <h2>Sing up</h2><br/>

            <label for="mail">E-mail:</label><br/>
            <input type="email" id="mail" name="user_email"><br/>

            <div class="left">
                <label for="password">Password:</label><br/>
                <input type="password" id="password" name="user_password">
                <label for="confirm">Confirm:</label><br/>
                <input type="password" id="confirm" name="user_confirm">


            </div>
            <br/>

        <button><b>Sing up</b></button>

    </form>
</div>
<div id="footer">&copy; Vova</div>
</body>
</html>
