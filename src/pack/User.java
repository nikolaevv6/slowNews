package pack;

import java.util.ArrayList;

/**
 * Created by voldem on 10.11.2015.
 */
public class User {
    String email;
    String pass;
    ArrayList<User> allUsers;

    public User(String email, String pass) {
        this.email = email;
        this.pass = pass;

    }

    @Override
    public String toString() {
        return "User{" +
                "pass='" + pass + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public ArrayList<User> getAllUsers() {
        return allUsers;
    }

    public void setAllUsers(ArrayList<User> allUsers) {
        this.allUsers = allUsers;
    }

    public String getEmail() {
        return email;
    }

    public String getPass() {
        return pass;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
